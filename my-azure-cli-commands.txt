Azure-cli Linux

Installation tutorial:
RHEL / Almalinux / Rocky Linux
https://learn.microsoft.com/en-us/cli/azure/install-azure-cli-linux?pivots=dnf

Tutorial: usage
https://learn.microsoft.com/en-us/cli/azure/get-started-with-azure-cli

steps for ALMALINUX 8 / RHEL8:
sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc
sudo dnf install -y https://packages.microsoft.com/config/rhel/8/packages-microsoft-prod.rpm
sudo dnf install azure-cli -y

as normal user check version:
az version

azure-cli login/logout:
az login
az logout

azure list resource groups:
az group list

azure subscription details:
az account show

azure-cli upgrade:
az upgrade

enable auto-upgrade: 
az config set auto-upgrade.enable=yes
