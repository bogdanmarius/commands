WEBTOP, docker containers with desktop 
website: https://docs.linuxserver.io/images/docker-webtop

MODELS of docker containers with desktop environment

1. webtop desktop, using latest version:
default is XFCE Alpine
HINT: create a volume location for the container data: 
cd /home/username/
mkdir webtop
cd webotp ; mdkir config

docker run -d \
  --name=webtop-xfce-alpine \
  --security-opt seccomp=unconfined \
  -e PUID=1000 \
  -e PGID=1000 \
  -e TZ=Etc/UTC \
  -e TITLE=Webtop \
  -p 3000:3000 \
  -p 3001:3001 \
  -v /home/username/webtop/config:/config \
  -v /var/run/docker.sock:/var/run/docker.sock \
  --device /dev/dri:/dev/dri \
  --shm-size="1gb" \
  --restart unless-stopped \
  lscr.io/linuxserver/webtop:latest

 
2. webtop desktop, Alpine linux with Mate desktop:

HINT: create a volume location for the container data: 
cd /home/username/
mkdir webtop
cd webotp ; mdkir config

docker run -d \
  --name=webtop-mate-alpine \
  --security-opt seccomp=unconfined \
  -e PUID=1000 \
  -e PGID=1000 \
  -e TZ=Etc/UTC \
  -e TITLE=Webtop-mate-desktop \
  -p 3000:3000 \
  -p 3001:3001 \
  -v /home/username/webtop/config:/config \
  -v /var/run/docker.sock:/var/run/docker.sock \
  --device /dev/dri:/dev/dri \
  --shm-size="1gb" \
  --restart unless-stopped \
  lscr.io/linuxserver/webtop:alpine-mate
  
3. webtop desktop, Alpine linux with KDE desktop:

HINT: create a volume location for the container data: 
cd /home/username/
mkdir webtop
cd webotp ; mdkir config

docker run -d \
  --name=webtop-kde-alpine \
  --security-opt seccomp=unconfined \
  -e PUID=1000 \
  -e PGID=1000 \
  -e TZ=Etc/UTC \
  -e TITLE=Webtop-kde-desktop \
  -p 3000:3000 \
  -p 3001:3001 \
  -v /home/username/webtop/config:/config \
  -v /var/run/docker.sock:/var/run/docker.sock \
  --device /dev/dri:/dev/dri \
  --shm-size="1gb" \
  --restart unless-stopped \
  lscr.io/linuxserver/webtop:alpine-kde  
