KUBERNETES:

official cheat sheet:
https://kubernetes.io/docs/reference/kubectl/cheatsheet/

HINT: USE --- to separate resources inside a single yaml file

KUBERNETES NAMESPACES:

create kubernetes namespace
kubectl create namespace desired-name-of-namespace

list all namespaces:
kubectl get namespaces

add a kubernetes pods to a certain namespace:
kubectl apply --namespace=desired-name-of-namespace -f pod-definition-file.yml

list pods from a custom namespace:
kubectl -n namespace-name get pods

delete a namespace:
kubectl delete namespace namespace-name

KUBERNETES PODS:

start a kubernetes pod, using a docker image:
kubectl run pod-name --image=image-name-from-docker
example: kubectl run nginx --image=nginx
example2: kubectl run nginx --image-nginx --port=5701

kubectl list pods:
kubect get pods

kubectl list pods, using wide option:
kubectl get pods -o wide

describe pods ( list complete details about a pod):
kubectl describe pod pod-name
example: kubectl describe pod nginx

access a certain pod via shell:
kubectl get pods
method 1: kubectl exec -ti pod-name -- bash
method 2: kubectl exec -ti pod-name -n namespace-name -- /bin/sh
method 3: kubectl exec --stdin --tty pod-name -- /bin/bash
MORE on pod access: https://kubernetes.io/docs/tasks/debug/debug-application/get-shell-running-container/

start a single instance of busybox and keep it in the foreground, don't restart it if it exits.
kubectl run -i --tty busybox --image=busybox --restart=Never
MORE on kubectl run: https://jamesdefabia.github.io/docs/user-guide/kubectl/kubectl_run/

delete a created pod:
kubectl get pods
kubectl delete pod pod-name

view pod logs (includes all containers)
kubectl logs pod-name

tail pod logs:
kubectl logs -f pod-name --all-containers=true

show all logs from pod written in the last hour
kubectl logs --since=1h pod-name

KUBERNETES PODS with YAML file:

BASIC STRUCTURE:
example name: pod-definition.yml OR pod-definition.yaml
WARNING: ALWAYS USE SPACES in the structure for indentation


pod-definition.yml structure 
(using ============== to delimit the structure in the example)
==============
apiVersion: 

kind:

metadata:

spec:

==============


below example of an kubernetes pod using the nginx image
EXAMPLE:
==============
apiVersion: v1
kind: Pod
metadata:
  name: myapp-pod
  labels: 
      app: myapp
	  type: example-front-end
spec:
  containers:
    - name: nginx-container
	  image: nginx

==============

create pod using yaml file in kubernetes (useful for updating as well):
kubectl apply -f pod-definition.yml

create yaml file using kubectl run:
kubectl run name-of-pod --image=image-name-from-docker --dry-run=client -o yaml > filename.yaml


KUBERNETES PODS WITH REPLICATION CONTROLLER

yaml file with replication controller (has 3 initial replicas):
EXAMPLE:
==============
apiVersion: v1
kind: ReplicationController
metadata:
  name: myapp-rc
  labels: 
      app: myapp
	  type: front-end
spec: 
  template:
     metadata:
	  name: myapp-pod
	  labels: 
	     app: myapp
		 type: front-end
	 spec:
	   containers:
	     - name: nginx-container
		   image: nginx
  replicas: 3
==============
create the replication controller:
kubectl apply -f filename-with-rc-setup.yml

list number of replicas from the controller: 
kubectl get replicationcontroller

KUBERNETES PODS WITH REPLICA SET
	  
yaml file with replica set (has 3 initial replicas):
EXAMPLE:
==============	  
apiVersion: apps/v1
kind: ReplicaSet
metadata:
  name: myapp-replicaset
  labels: 
      app: myapp
	  type: front-end
spec: 
  template:
     metadata:
	  name: myapp-pod
	  labels: 
	     app: myapp
		 type: front-end
	 spec:
	   containers:
	     - name: nginx-container
		   image: nginx
  replicas: 3
  selector:
     matchLabels:
	    type: front-end
==============
WARNING: selector -> matchLabels MUST ALWAYS MATCH spec -> template -> metadata -> labels

create the replica set:
kubectl apply -f filename-ReplicaSet-setup.yml

list number of replicas from the set: 
kubectl get replicaset

update the replica set with a higher number of replicas:
edit the number of replicas from the yml file, we add 6 replicas in this example:
EXAMPLE:
==============	  
apiVersion: apps/v1
kind: ReplicaSet
metadata:
  name: myapp-replicaset
  labels: 
      app: myapp
	  type: front-end
spec: 
  template:
     metadata:
	  name: myapp-pod
	  labels: 
	     app: myapp
		 type: front-end
	 spec:
	   containers:
	     - name: nginx-container
		   image: nginx
  replicas: 6
  selector:
     matchLabels:
	    type: front-end
==============
execute: kubectl replace -f filename-ReplicaSet-setup.yml to update the replica set

!!! without editing the yaml file:
alternative: kubectl scale --replicas=6 -f filename-ReplicaSet-setup.yml

alternative 2 (interactive editor): kubectl edit replicaset name-of-replicaset
search and update the value from replicas to the desired number

list all replicasets:
kubectl get replicasets

describe replica set:
kubectl describe replicaset name-of-replicaset

kubectl explain replicaset (used to identify the apiVersion parameter for the yaml file):
kubectl explain replicaset
find VERSION in the output to be used in the yaml file

delete replicaset ( !!! this will delete pods as well !!! ):
kubectl delete replicaset name-of-replicaset (from the yaml file)

KUBERNETES PODS WITH DEPLOYMENT

create pods + deployment:
kubectl apply deployment name-of-deployment --image=image-name-from-Docker
example: kubectl apply deployment name-of-deployment --image=nginx

expose deployment for outside traffic:
kubectl expose deployment name-of-deployment --type="NodePort" --port 80

delete a kubernetes deployment:
kubectl get deployments
kubectl delete deployment deployment-name

yaml file with deployment definition:
EXAMPLE:
==============	  
apiVersion: apps/v1
kind: Deployment
metadata:
  name: myapp-deployment
  labels: 
      app: myapp
	  type: front-end
spec: 
  template:
     metadata:
	  name: myapp-pod
	  labels: 
	     app: myapp
		 type: front-end
	 spec:
	   containers:
	     - name: nginx-container
		   image: nginx
		   imagePullPolicy: Always
		   livenessProbe:
		     httpGet:
			   path: /
			   port: 80 # (this is the application port from the pod)
			 periodSeconds: 10
			 initialDelaySeconds: 5
  replicas: 3
  selector:
     matchLabels:
	    type: front-end
==============
MORE on Configure Liveness, Readiness and Startup Probes:
https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes/

MORE on Deployments models:
https://kubernetes.io/docs/concepts/workloads/controllers/deployment/

create deployment: 
kubectl apply -f deployment-file.yaml

create deployment and record details:
kubectl apply -f deployment-file.yaml --record

create deployment using multiple yaml files
kubectl apply -f=name-of-deployment-file.yaml -f=name-of-other-deployment-file.yaml

create deployment using command line:
kubectl apply deployment name-of-deployment --image==image-name-from-docker --replicas=number-of-desired-replicas

create deployment in a certain namespace:
kubectl apply deployment name-of-deployment --image=image-name-from-docker -n=name-of-namespace

expose the port of the deployment:
kubectl expose deployment name-of-deployment --port=port-number --type=LoadBalancer

list all deplyments:
kubectl get deployments

get a list of all created kubernetes objects:
kubectl get all

display list of deployments and details
kubectl rollout status deployment/name-of-deployment
kubectl rollout history deployment/name-of-deployment

EXAMPLE 2 
kubectl deployment model
here we insert a desired version of the image in spec - containers - image

EXAMPLE:
==============	  
apiVersion: apps/v1
kind: Deployment
metadata:
  name: myapp-deployment
  labels: 
      app: myapp
	  type: front-end
spec: 
  template:
     metadata:
	  name: myapp-pod
	  labels: 
	     app: myapp
		 type: front-end
	 spec:
	   containers:
	     - name: nginx-container
		   image: nginx:1.7.1
  replicas: 3
  selector:
     matchLabels:
	    type: front-end
==============
execute changes and deploy: 
kubectl apply -f name-of-deployment-file.yml

check the deployment status: 
kubectl describe deployment name-of-deployment

Alternative to edit and update the deployment-file:
kubectl edit deployment name-of-deployment --record
-> here we insert a desired version of the image in spec - containers - image
Save the the file and check status of deployment:
kubectl rollout status deployment/name-of-deployment

deployment rollback
kubectl rollout undo deployment/name-of-deployment

deployment deletion:
kubectl delete deployment name-of-deployment

deployment deletion using yaml file
kubectl detele -f name-of-deployment-file.yaml 

KUBERNETES SERVICES

create the service definition file
in the selector zone we have the labels from pod-definition app and type
EXAMPLE:
=====
apiVersion: v1
kind: Service
metadata:
    name: myapp-service
spec:
	type: LoadBalancer 
	# type: NodePort (alternative option)
	ports:
	 - protocol: 'TCP'
	   port: 80
	   targetPort: 80
	   # nodePort: 30008 (if using type: NodePort )
	selector:
	   app: myapp
	   type: front-end
=====
create the service: 
kubectl apply -f name-of-service-file.yml

list available services: 
kubectl get services

delete a kubernetes service:
kubectl get services
kubectl delete services <service-name>

KUBERNETES SERVICE with CLUSTERIP 

cluster ip definition file
EXAMPLE:
=====
apiVersion: v1
kind: Service
metadata:
    name: back-end
	
spec:
    type: ClusterIP
	ports:
	 - targetPort: 80
	   port: 80
	   
	selector:
	   app: my-app
	   type: back-end
=====
create the service: 
kubectl apply -f name-of-definition-file.yml

list services: 
kubectl get services

KUBERNETES SERVICE with LOADBALANCER

load balancer definition file example:
EXAMPLE:
=====
apiVersion: v1
kind: Service
metadata:
    name: myapp-service
	
spec:
    type: LoadBalancer
	ports:
	  - targetPort: 80
	    port: 80
		nodePort: 30008
		
=====
create the service: 
kubectl apply -f name-of-definition-file.yml

list services: 
kubectl get services
