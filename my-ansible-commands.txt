Setup was done on Almalinux 8

REQUIREMENTS
user with sudo access

INSTALLATION: 
sudo dnf update
sudo dnf install epel-release
sudo dnf install ansible


GENERATE SSH KEYS to be deployed on the target servers
create local key with the command:
ssh-keygen

deploy the public key to each target server:
example: ssh-copy-id ansible@172.17.0.3 (username and ip address could be different for you)

add the ansible user to sudoers to login without password, on each target server:
echo "$(whoami) ALL=(ALL) NOPASSWD:ALL" | sudo tee /etc/sudoers.d/$(whoami)

USE tutorial: https://linux.how2shout.com/how-to-install-ansible-on-rocky-linux-8-or-almalinux/
for ssh key configuration steps for the target hosts

POST INSTALLATION CHECK:
ansible --version

LOCAL INVENTORY
create ansible inventory of hosts
sudo vim /etc/ansible/hosts

BASIC ANSIBLE COMMANDS
ping: ansible -m ping group-name (group-name defined in /etc/ansible/hosts, default is webservers)
      ansible -m ping ip-address (ip-address defined in /etc/ansible/hosts)
	  ansible -m ping all
	  
COMMANDS WITH SUDO
structure:
ansible -b --become-method=sudo -m shell -a 'command to execute' group-name

examples: 
ansible -b --become-method=sudo -m shell -a 'dnf install iptraf-ng' webservers* ( installs a package on the servers )
ansible -b --become-method=sudo -m shell -a 'curl -I -L domain.tld' webservers* (the servers will send a curl command to the host)
*in this case webservers is the group name that holds the assigned target hosts

COMMANDS WITHOUT SUDO
structure
ansible -m command -a "uptime" group-name/ip-adress

examples:
keep in mind that group name (webservers) or ip addresses (like 172.17.0.2) could be different
ansible -m command -a "uptime" webservers
ansible -m command -a "uptime" 172.17.0.3
ansible -m command -a "ps -aux" webservers
ansible -m command -a "ps -aux" 172.17.0.3
*in this case the ip addresses are from Docker containers

MORE details and examples:
https://docs.ansible.com/ansible/latest/index.html
https://docs.ansible.com/ansible/latest/inventory_guide/intro_inventory.html
https://docs.ansible.com/ansible/latest/playbook_guide/index.html
https://docs.ansible.com/ansible/latest/getting_started/get_started_playbook.html
https://www.golinuxcloud.com/ansible-playbook-examples/
https://thenathan.net/2020/07/16/yum-and-dnf-update-and-reboot-with-ansible/
https://devopscube.com/ansible-playbook-examples/


CREATE ANSIBLE INVENTORY
create the yaml file in the home directory (path /home/username)
vim inventory.yaml (don`t use dashes in the name)

example content of inventory.yaml:
dockercontainers:
  hosts:
    docker1:
      ansible_host: 172.17.0.2
    docker2:
      ansible_host: 172.17.0.3
	  
dockercontainers - the name of group for the resources
docker1,docker2 - the friendly name of the VM/container
(local ip addresses defined for the docker containers)	 
 

VALIDATE(VERIFY) THE INVENTORY:
ansible-inventory -i inventory.yaml --list


CHECK THE STATUS of the servers added in the inventory:
ansible dockercontainers -m ping -i inventory.yaml


INVENTORIES AND PLAYBOOKS, job execution:
dry run: ansible-playbook -i inventories/inventory.yaml playbooks/playbook.yaml -C
actual run: ansible-playbook -i inventories/inventory.yaml playbooks/playbook.yaml
troubleshooting: ansible-playbook -vvv

TEST CASE
testing ansible-playbook command
example:
vim playbook-print-message.yaml

---
 - hosts: all
   tasks:
   - debug:
       msg:
        - "This is first line"
        - "This is second line"
        - "This is third line"
		
vim inventory.yaml
dockercontainers:
  hosts:
    docker1:
      ansible_host: 172.21.0.2
      ansible_user: ansible
    docker2:
      ansible_host: 172.21.0.3
      ansible_user: ansible
	  
execution of playbook using the inventory
ansible-playbook -i inventory.yaml playbook-print-message.yaml (first file is the inventory, second file is the playbook)
ansible-playbook -i inventory.yaml playbook-print-message.yaml -v (add more verbose details)
