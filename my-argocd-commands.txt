ARGOCD commands

Requirements for minikube setup:
docker
minikube
kubectl repo and tool installed

INSTALL ARGOCD SERVER

install ArgoCD in k8s:
kubectl create namespace argocd
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml

Change the argocd-server service type to LoadBalancer to have external IP:
kubectl patch svc argocd-server -n argocd -p '{"spec": {"type": "LoadBalancer"}}'

login with admin user and below token (as in documentation):
kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 --decode && echo

access ArgoCD UI
kubectl get svc -n argocd
kubectl port-forward svc/argocd-server 9090:443 -n argocd

INSTALL ARGOCD CLI
curl -sSL -o argocd-linux-amd64 https://github.com/argoproj/argo-cd/releases/latest/download/argocd-linux-amd64
sudo install -m 555 argocd-linux-amd64 /usr/local/bin/argocd
rm argocd-linux-amd64

login to the argocd server from CLI:
argocd login 127.0.0.1:9090

get CLI and SERVER version:
argocd version

create the app-demo from the git repository:
argocd app create app-demo --repo https://gitlab.com/bogdanmarius/argocd.git --path app-demo --dest-server https://kubernetes.default.svc --dest-namespace default

check the status of the demo app
argocd app get app-demo

sync the app-demo repo and create the deployment and service:
argocd app sync app-demo

list argocd deployments:
argocd app list