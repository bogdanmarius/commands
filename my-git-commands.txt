How to GIT:

Here are few steps to work with GIT pushing changes to server. 

We are using git bash to perform these operations on Windows.
On Linux you might need to use something like sudo dnf install git (RHEL like distros) / sudo apt install git (Debian like distros)

-- Clone the REPO to local box folder. Example: "/e/Workspace" on Windows
-- Repo structure will be downloaded into "repo-name" folder, example with urbanterror-cfg repository for Bitbucket or games for GitLab
$ git clone https://bogdanmarius-admin@bitbucket.org/bogdanmarius/urbanterror-cfg.git (bitbucket )
$ git clone https://gitlab.com/bogdanmarius/games.git (gitlab)

-- go to urbanterror-cfg folder
$ cd /e/Workspace/urbanterror-cfg

-- get latest changes
$ git pull

--get status
$ git status

-- Switch to your branch (new branch):
Use "feature/*" for new modifications, or use "bugfix/*" for changes in existing files, in Bitbucket.
Also BMU is a project key code, your might be differet in Bitbucket or absent at all.
$ git checkout -b feature/<JIRA Ticket "BMU-****"> ( for Bitbucket branch creation with Jira integration)
$ git checkout -b feature/issues-#1 ( issues number # from website, for GitLab branch creation )

-- check if you are on created branch:
$ git status

-- Edit file or copy new file in git directory, below example shows COPY procedure, for edit existing file use VI / VIM.
-- In example below we perform copy from users desktop to <curent folder>:
$ cp /c/users/<USER>/Desktop/<filename>.json (or any other extension <filename>.sh, <filename>.py, <filename>.txt )

-- Update remote branch by adding file or folder:
$ git add <filename>.json
or folder
$ git add folder/* 

-- Commit changes adding "message" for approvers!
$ git commit -m "BMU-1 File with new options" ( for Bitbucket commit )
$ git commit -m "Update of the readme file. Ref #1" ( for GitLab commit, #xxx - issue number )

-- Push changes to server:
$ git push -u origin feature/BMU-1 (bitbucket)
$ git push origin feature/issues-#1 (gitlab)

-- Check the changes after they are merged:
$ git checkout main (gitlab)

-- go to Bitbucket/GitLab and create pull request.

-- Create git tags:
$ git tag -a v1.0 -m "Version 1.0" ( In this short version, the annotated tag's name is "v1.0", and the message is "Version 1.0". )
$ git tag -a v1.0 ( Use this version to write a longer tag message for annotated tag "v1.0" in your text editor. )

-- Push git tags:
$ git push origin --tags

=== How to delete local branch and remote branch ===

-- delete remote branch
git push origin -d <remote-branch-name>

-- Switch from your branch to master:
git checkout master

-- delete local branch:
git branch -d <local-branch-name>
-- delete local branch using force parameter:
git branch -D <local-branch-name>
