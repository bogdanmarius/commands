# commands

## Cloning the repository

```
cd existing_repo
git clone https://gitlab.com/bogdanmarius/commands.git
cd commands
```

## Name
Repository name commands.

## Description
This repository is created to host cheat sheets in text file format.

## Project status
Project status is Live.
